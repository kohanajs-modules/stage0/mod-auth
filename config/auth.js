const { KohanaJS } = require('kohanajs');

module.exports = {
  databasePath: `${KohanaJS.EXE_PATH}/../database`,
  userDatabase: 'user.sqlite',
  salt: 'thisislonglonglonglongtextover32bytes',

  destination: '/account',
  requireActivate: true,
  rootRole: 'root',

  identifiers: [],
  verified: {}
};
