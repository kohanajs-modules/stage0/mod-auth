/* Controller auth handle login, logout */
const { Controller } = require('@kohanajs/core-mvc');
const { ControllerMixinDatabase, ControllerMixinMime, ControllerMixinView, KohanaJS } = require('kohanajs');
const { ControllerMixinMultipartForm } = require('@kohanajs/mod-form');
const { ControllerMixinSession } = require('@kohanajs/mod-session');

const ControllerMixinAuth = require('../controller-mixin/Auth');

class ControllerAuth extends Controller {
  static mixins = [...Controller.mixins,
    ControllerMixinMultipartForm,
    ControllerMixinDatabase,
    ControllerMixinSession,
    ControllerMixinAuth,
    ControllerMixinMime,
    ControllerMixinView
  ]

  constructor(request) {
    super(request);

    this.state.get(ControllerMixinDatabase.DATABASE_MAP)
      .set('session', `${KohanaJS.config.auth.databasePath}/session.sqlite`)
      .set('admin', `${KohanaJS.config.auth.databasePath}/${KohanaJS.config.auth.userDatabase}`);
  }

  async action_login() {
    this.setTemplate('templates/login', {
      destination: this.request.query.cp || KohanaJS.config.auth.destination,
      message: '',
    });
  }

  async action_login_post() {}

  async action_fail() {
    this.setTemplate('templates/login', {
      destination: this.request.query.cp,
      message: 'Login fail.',
    });
  }

  async action_logout() {
    this.setTemplate('templates/login', {
      destination: KohanaJS.config.auth.destination,
      message: 'User Log Out Successfully.',
    });
  }
}

module.exports = ControllerAuth;
